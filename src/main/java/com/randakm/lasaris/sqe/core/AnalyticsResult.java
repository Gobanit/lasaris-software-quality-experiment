package com.randakm.lasaris.sqe.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used as a container for multiple analytics result items.
 *
 */
public class AnalyticsResult implements Serializable {
  private static final long serialVersionUID = 4643549016520180377L;

  private List<AnalyticsResultItem> analyticsItems = new ArrayList<>();

  public void addItem(AnalyticsResultItem item) {
    analyticsItems.add(item);
  }

  /**
   * Getter for analyticsItems.
   *
   * @return the analyticsItems
   */
  public List<AnalyticsResultItem> getAnalyticsItems() {
    return analyticsItems;
  }

  /**
   * Setter for analyticsItems.
   *
   * @param analyticsItems - the analyticsItems to set
   */
  public void setAnalyticsItems(List<AnalyticsResultItem> analyticsItems) {
    this.analyticsItems = analyticsItems;
  }

  @Override
  public String toString() {
    return "AnalyticsResult [analytics =" + analyticsItems + "]";
  }

}
