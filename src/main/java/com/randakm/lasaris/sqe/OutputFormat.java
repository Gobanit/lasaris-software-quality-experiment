package com.randakm.lasaris.sqe;

/**
 * Enumeration of possible output formats.
 *
 */
public enum OutputFormat {
  XML, JSON, PLAIN;
}
