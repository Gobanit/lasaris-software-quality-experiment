package com.randakm.lasaris.sqe;

import com.randakm.lasaris.sqe.core.AnalyticsResult;
import com.randakm.lasaris.sqe.core.ManipulationMethod;
import com.randakm.lasaris.sqe.core.Order;
import com.randakm.lasaris.sqe.serializing.AnalyticsResultSerializer;
import com.randakm.lasaris.sqe.serializing.JsonAnalyticsResultSerializer;
import com.randakm.lasaris.sqe.serializing.ReflectionAnalyticsResultSerializer;
import com.randakm.lasaris.sqe.serializing.XMLAnalyticsResultSerializer;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Main class, starting the spring boot and also the command line endpoint,
 * contains the main logic.
 *
 */
@SpringBootApplication
public class SoftwareQualityExperimentApplication implements CommandLineRunner {
  private static final Logger LOGGER = LoggerFactory.getLogger(SoftwareQualityExperimentApplication.class);

  @Autowired
  private ApplicationContext appContext;

  public static void main(String[] args) throws IOException, ParseException {
    SpringApplication.run(SoftwareQualityExperimentApplication.class, args);
  }

  /**
   * Starts the command line application
   */
  @Override
  public void run(String... args) throws Exception {
    CommandLine cmd = configureCmd(args);
    run(cmd);
  }

  private CommandLine configureCmd(String[] args) throws ParseException {
    Options options = new Options();

    Option o = new Option("d", "path to remote or local CSV file.");
    o.setArgs(1);
    o.setRequired(true);
    options.addOption(o);

    o = new Option("m", "list of methods (filter/analytics) to apply");
    o.setArgs(Option.UNLIMITED_VALUES);
    o.setRequired(true);
    options.addOption(o);

    o = new Option("o", "output format and path to file");
    o.setArgs(2);
    o.setRequired(true);
    options.addOption(o);

    CommandLineParser clparser = new DefaultParser();
    CommandLine cmd = clparser.parse(options, args);
    return cmd;
  }

  private void run(CommandLine cmd) throws MalformedURLException, IOException, ParseException, ClassNotFoundException {
    // print arguments
    LOGGER.info("d value: " + cmd.getOptionValue("d"));
    LOGGER.info("m value: " + Arrays.deepToString(cmd.getOptionValues("m")));
    LOGGER.info("o value: " + Arrays.deepToString(cmd.getOptionValues("o")));

    // parse arguments
    String dataSetUrl = cmd.getOptionValue("d");
    OutputFormat outputFormat = OutputFormat.valueOf(cmd.getOptionValues("o")[0].toUpperCase());
    String outputFilePath = cmd.getOptionValues("o")[1];

    AnalyticsResultSerializer serializer = chooseSerializer(outputFormat);

    // configure to-be used methods
    List<ManipulationMethod> methodsChain = new ArrayList<>();

    String[] methods = cmd.getOptionValues("m");
    for (String m : methods) {
      if (!appContext.containsBean(m)) {
        throw new IllegalArgumentException("No manipulation method '" + m + "' is defined");
      }
      ;
      ManipulationMethod mm = appContext.getBean(m, ManipulationMethod.class);
      methodsChain.add(mm);
    }

    // load data
    List<Order> orders = loadDataset(dataSetUrl);

    // execute manipulation methods
    AnalyticsResult analyticsResult = new AnalyticsResult();
    for (ManipulationMethod mm : methodsChain) {
      LOGGER.info("Apply method " + mm.getClass());
      orders = mm.execute(orders, analyticsResult);
      LOGGER.info("Current number of orders: " + orders.size());
    }

    // generate output
    String output = serializer.serialize(analyticsResult);
    LOGGER.info("Output: \n" + output);

    LOGGER.info("Writing output to " + new File(outputFilePath).getAbsolutePath());
    FileUtils.writeStringToFile(new File(outputFilePath), output, "UTF-8");
  }

  private AnalyticsResultSerializer chooseSerializer(OutputFormat outputFormat) {
    switch (outputFormat) {
      case JSON:
        return new JsonAnalyticsResultSerializer();
      case PLAIN:
        return new ReflectionAnalyticsResultSerializer();
      case XML:
        return new XMLAnalyticsResultSerializer();
      default:
        throw new IllegalArgumentException("No serializer available for " + outputFormat + " format yet");
    }

  }

  private List<Order> loadDataset(String dataSetUrl) throws MalformedURLException, IOException {
    InputCSVParser parser = new InputCSVParser();
    List<Order> orders = parser.loadFromUrl(dataSetUrl);
    LOGGER.info("Total number of orders: " + orders.size());
    for (Order o : orders) {
      LOGGER.debug("" + o);
    }

    return orders;
  }

}
