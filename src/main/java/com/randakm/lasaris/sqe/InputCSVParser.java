package com.randakm.lasaris.sqe;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.randakm.lasaris.sqe.core.Order;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class responsible for parsing the input csv to list of orders.
 *
 */
public class InputCSVParser {
  private static final Logger LOGGER = LoggerFactory.getLogger(SoftwareQualityExperimentApplication.class);
  private static final String ENCODING = "UTF-8";

  /**
   * Loads the csv from filepath on disc.
   * 
   * @param filePath - file location
   * @return list of parsed orders
   * @throws IOException - thrown if problem occured while reading file.
   */
  public List<Order> loadFromFile(String filePath) throws IOException {
    return loadFromUrl(new File(filePath).toURI().toString());
  }

  /**
   * Loads the csv from url.
   * 
   * @param url - location of csv file
   * @return list of parsed orders
   * @throws MalformedURLException - thrown if url is not valid.
   * @throws IOException           - thrown if problem occured while reading file.
   */
  public List<Order> loadFromUrl(String url) throws MalformedURLException, IOException {
    LOGGER.info("Loading data from '" + url + "'");
    BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());
    String data = IOUtils.toString(in, ENCODING);
    in.close();
    return load(data);
  }

  private List<Order> load(String data) {
    // add header, since the csv does not contain it
    String header = "order_id,order_date,customer_email,customer_address,total_price,order_status";
    data = header + "\n" + data;

    List<Order> orders = new ArrayList<>();

    HeaderColumnNameMappingStrategy<Order> strategy = new HeaderColumnNameMappingStrategy<>();
    strategy.setType(Order.class);

    StringReader sr = new StringReader(data);
    CsvToBean<Order> csvToBean = new CsvToBeanBuilder<Order>(sr).withMappingStrategy(strategy)
        .withIgnoreLeadingWhiteSpace(true).withSeparator(',').build();

    orders = csvToBean.parse();
    sr.close();    

    return orders;
  }
}
