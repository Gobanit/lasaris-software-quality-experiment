package com.randakm.lasaris.sqe.methods.filters;

import com.randakm.lasaris.sqe.core.FilteringMethod;
import com.randakm.lasaris.sqe.core.Order;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Simple implementation of filter, which keeps only the items fulfilling
 * condition of {@link SimpleAbstractFilter#shouldKeep(Order)} method
 *
 */
abstract public class SimpleAbstractFilter extends FilteringMethod {

  /**
   * Filters input and keeps only orders fulfilling specific condition of
   * {@link SimpleAbstractFilter#shouldKeep(Order)}.
   */
  @Override
  protected List<Order> filter(List<Order> orders) {
    return orders.stream().filter((o) -> shouldKeep(o)).collect(Collectors.toList());
  }

  /**
   * Decides whether specific order should be kept in output list, or not.
   * 
   * @param o - order
   * @return whether order should be kept
   */
  abstract boolean shouldKeep(Order o);

}
