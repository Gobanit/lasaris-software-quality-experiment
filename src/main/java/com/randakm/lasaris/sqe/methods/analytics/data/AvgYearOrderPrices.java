package com.randakm.lasaris.sqe.methods.analytics.data;

import com.randakm.lasaris.sqe.core.AnalyticsResultItem;
import com.randakm.lasaris.sqe.core.OrderStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Analytics result item containing average prices of orders with specific
 * status per year.
 *
 */
public class AvgYearOrderPrices extends AnalyticsResultItem {
  private static final long serialVersionUID = 4747358828593526203L;

  private List<YearAndPrice> avgPrices = new ArrayList<>();
  private OrderStatus status;

  @Override
  public String getName() {
    return "Average prices per year for orders with status " + status;
  }

  /**
   * Getter for status.
   *
   * @return the status
   */
  public OrderStatus getStatus() {
    return status;
  }

  /**
   * Setter for status.
   *
   * @param status - the status to set
   */
  public void setStatus(OrderStatus status) {
    this.status = status;
  }

  /**
   * Getter for avgPrices.
   *
   * @return the avgPrices
   */
  public List<YearAndPrice> getAvgPrices() {
    return avgPrices;
  }

  /**
   * Setter for avgPrices.
   *
   * @param avgPrices - the avgPrices to set
   */
  public void setAvgPrices(List<YearAndPrice> avgPrices) {
    this.avgPrices = avgPrices;
  }

}
