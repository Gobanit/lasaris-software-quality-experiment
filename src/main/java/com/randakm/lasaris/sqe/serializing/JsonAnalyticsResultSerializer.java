package com.randakm.lasaris.sqe.serializing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.randakm.lasaris.sqe.OutputFormat;
import com.randakm.lasaris.sqe.core.AnalyticsResult;

import org.apache.commons.lang3.SerializationException;

/**
 * Class serializing {@link AnalyticsResult} to some JSON format.
 *
 */
public class JsonAnalyticsResultSerializer implements AnalyticsResultSerializer {

  @Override
  public String serialize(AnalyticsResult result) {
    ObjectMapper objectMapper = new ObjectMapper();
    String json;
    try {
      json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);
    } catch (JsonProcessingException e) {
      throw new SerializationException(e);
    }
    return json;
  }

  @Override
  public OutputFormat getSupportedFormat() {
    return OutputFormat.JSON;
  }

}
