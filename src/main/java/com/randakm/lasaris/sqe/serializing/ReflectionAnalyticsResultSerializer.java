package com.randakm.lasaris.sqe.serializing;

import com.randakm.lasaris.sqe.OutputFormat;
import com.randakm.lasaris.sqe.core.AnalyticsResult;

import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * Class serializing {@link AnalyticsResult} by recursive toString calls using
 * apache commons library.
 *
 */
public class ReflectionAnalyticsResultSerializer implements AnalyticsResultSerializer {

  @Override
  public String serialize(AnalyticsResult result) {
    return new ReflectionToStringBuilder(result, new RecursiveToStringStyle()).toString();
  }

  @Override
  public OutputFormat getSupportedFormat() {
    return OutputFormat.PLAIN;
  }

}
