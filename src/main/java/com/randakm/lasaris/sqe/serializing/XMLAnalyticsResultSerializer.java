package com.randakm.lasaris.sqe.serializing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.randakm.lasaris.sqe.OutputFormat;
import com.randakm.lasaris.sqe.core.AnalyticsResult;

import org.apache.commons.lang3.SerializationException;

public class XMLAnalyticsResultSerializer implements AnalyticsResultSerializer {

  @Override
  public String serialize(AnalyticsResult result) {
    ObjectMapper objectMapper = new XmlMapper();
    String json;
    try {
      json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);
    } catch (JsonProcessingException e) {
      throw new SerializationException(e);
    }
    return json;

  }

  @Override
  public OutputFormat getSupportedFormat() {
    return OutputFormat.XML;
  }

}
